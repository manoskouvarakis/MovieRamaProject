﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MovieRama.Models
{
    public class Impression : Model
    {
		public enum ImpressionType
		{
			Like = 0,
			Hate = 1
		}

		public int ID { get; set; }
		public ImpressionType Type { get; set; }

		[Required]
		public string UserID { get; set; }

		[Required]
		public int MovieID { get; set; }

		[Display(Name = "Added At")]
		public DateTime CreationTime { get; set; }

		public virtual ApplicationUser User { get; set; }

		public virtual Movie Movie { get; set; }
	}
}
