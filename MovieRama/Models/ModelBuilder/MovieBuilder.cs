﻿using MovieRama.Helpers;
using MovieRama.Models.MoviesViewModels;
using MovieRama.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Threading;
using System.Threading.Tasks;

namespace MovieRama.Models.ModelBuilder
{
    public class MovieBuilder
    {
		private readonly IPolicyService _policyService;
		private readonly HumanReadableDateTimeService _humanReadableDateTimeService;

		public MovieBuilder(IPolicyService policyService, HumanReadableDateTimeService humanReadableDateTimeService)
		{
			this._policyService = policyService;
			this._humanReadableDateTimeService = humanReadableDateTimeService;
		}

		public MovieViewModel Build(Movie model, IPrincipal principal)
		{
			String userId = principal.GetUserId();

			MovieViewModel viewModel = new MovieViewModel();
			viewModel.ID = model.ID;
			viewModel.Title = model.Title;
			viewModel.Description = model.Description;
			viewModel.UserID = model.UserID;
			viewModel.UserName = model.User.UserName;
			viewModel.CreationTime = model.CreationTime.ToLocalTime();
			viewModel.CreationTimeString = model.CreationTime.ToLocalTime().ToString(System.Threading.Thread.CurrentThread.CurrentCulture);
			viewModel.LastUpdateTime = model.LastUpdateTime;
			viewModel.LastUpdateTimeTicks = model.LastUpdateTime.Ticks;

			viewModel.Likes = model.Impressions.Where(x => !x.UserID.Equals(model.UserID) && x.Type == Impression.ImpressionType.Like).Count();
			viewModel.Hates = model.Impressions.Where(x => !x.UserID.Equals(model.UserID) && x.Type == Impression.ImpressionType.Hate).Count();
			viewModel.HasLiked = model.Impressions.Any(x => x.Type == Impression.ImpressionType.Like && x.UserID.Equals(userId));
			viewModel.HasHated = model.Impressions.Any(x => x.Type == Impression.ImpressionType.Hate && x.UserID.Equals(userId));
			viewModel.CanExpressImpression = this._policyService.CanExpressimpression(model, userId);
			viewModel.CanEdit = this._policyService.CanEdit(model, userId);

			viewModel.IsStored = model.IsStored;

			viewModel.HumanReadableCreationTime = this._humanReadableDateTimeService.ToHumanReadableString(model.CreationTime);

			return viewModel;
		}

		public IEnumerable<MovieViewModel> Build(IEnumerable<Movie> source, IPrincipal principal)
		{
			List<MovieViewModel> viewModels = new List<MovieViewModel>();
			foreach (Movie m in source)
			{
				MovieViewModel viewModel = this.Build(m, principal);
				viewModels.Add(viewModel);
			}
			return viewModels;
		}
	}
}
