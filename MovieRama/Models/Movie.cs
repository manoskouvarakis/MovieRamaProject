﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace MovieRama.Models
{
	public class Movie : Model
	{
		public Movie() { }

		public int ID { get; set; }

		[Required, MaxLength(50)]
		public string Title { get; set; }
		[Required, MaxLength(450)]
		public string Description { get; set; }
		[Required]
		public string UserID { get; set; }

		public DateTime CreationTime { get; set; }
		public DateTime LastUpdateTime { get; set; }

		public virtual ApplicationUser User { get; set; }

		public virtual ICollection<Impression> Impressions { get; set; }
	}
}
