﻿using MovieRama.DataAccess;
using MovieRama.Models.MoviesViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MovieRama.Models.Query
{
    public class MovieQueryModel
    {
		public enum OrderingType
		{
			Ascending = 0,
			Descending = 1
		}

		public enum OrderingParameter
		{
			Date,
			Likes,
			Hates
		}

		private static readonly String OrderingParameterDateString = "date";
		private static readonly String OrderingParameterLikesString = "likes";
		private static readonly String OrderingParameterHatesString = "hates";
		private static readonly String OrderingDirectionAscentingString = "asc";
		private static readonly String OrderingDirectionDescendingString = "desc";

		private OrderingType OrderDirection { get; set; }
		private OrderingParameter OrderParameter { get; set; }

		public String UserID { get; set; }
		public String SortParam { get; set; }
		public String SortDir { get; set; }
		public String SearchString { get; set; }

		public MovieQueryModel()
		{
			this.UserID = String.Empty;
			this.SearchString = String.Empty;
			this.SortParam = "likes";
			this.SortDir = "desc";
		}

		public MovieCriteria BuildCriteria()
		{
			MovieCriteria criteria = new MovieCriteria
			{
				UserId = this.UserID,
				Search = this.SearchString
			};

			//Check ordering parameter to be executed directly in the database
			OrderType orderType = this.OrderDirection == OrderingType.Ascending ? OrderType.Ascending : OrderType.Descending;
			if (this.OrderParameter == OrderingParameter.Date)
			{
				criteria.OrderingFields.Add(new OrderingField<Movie>() { EntityField = x => x.CreationTime, OrderType = orderType });
			}
			if (this.OrderParameter == OrderingParameter.Likes)
			{
				criteria.OrderingFields.Add(new OrderingField<Movie>() { EntityField = x => x.Impressions.Where(y => y.Type == Impression.ImpressionType.Like).Count(), OrderType = orderType });
			}
			if (this.OrderParameter == OrderingParameter.Hates)
			{
				criteria.OrderingFields.Add(new OrderingField<Movie>() { EntityField = x => x.Impressions.Where(y => y.Type == Impression.ImpressionType.Hate).Count(), OrderType = orderType });
			}

			return criteria;
		}

		public void HandleQueryParameters()
		{
			if (!String.IsNullOrEmpty(this.SortParam))
			{
				if (this.SortParam.Equals(MovieQueryModel.OrderingParameterDateString)) this.OrderParameter = OrderingParameter.Date;
				else if (this.SortParam.Equals(MovieQueryModel.OrderingParameterLikesString)) this.OrderParameter = OrderingParameter.Likes;
				else if (this.SortParam.Equals(MovieQueryModel.OrderingParameterHatesString)) this.OrderParameter = OrderingParameter.Hates;
			}

			if (!String.IsNullOrEmpty(this.SortDir))
			{
				if (this.SortDir.Equals(MovieQueryModel.OrderingDirectionAscentingString)) this.OrderDirection = OrderingType.Ascending;
				else if (this.SortDir.Equals(MovieQueryModel.OrderingDirectionDescendingString)) this.OrderDirection = OrderingType.Descending;
			}
		}

		public String OrderingParameterToString()
		{
			switch (this.OrderParameter)
			{
				case OrderingParameter.Date: return MovieQueryModel.OrderingParameterDateString;
				case OrderingParameter.Likes: return MovieQueryModel.OrderingParameterLikesString;
				case OrderingParameter.Hates: return MovieQueryModel.OrderingParameterHatesString;
				default: return MovieQueryModel.OrderingParameterDateString;
			}
		}

		public String OrderingDirectionToString()
		{
			switch (this.OrderDirection)
			{
				case OrderingType.Ascending: return MovieQueryModel.OrderingDirectionAscentingString;
				case OrderingType.Descending: return MovieQueryModel.OrderingDirectionDescendingString;
				default: return MovieQueryModel.OrderingDirectionAscentingString;
			}
		}
	}
}
