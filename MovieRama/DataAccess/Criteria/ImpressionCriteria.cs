﻿using Microsoft.EntityFrameworkCore;
using MovieRama.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MovieRama.DataAccess
{
	public class ImpressionCriteria : EntityCriteriaBase<Impression>
	{
		public IEnumerable<int> IDs { get; set; }
		public String UserId { get; set; }
		public int? MovieId { get; set; }

		public override IQueryable<Impression> Collect(DbSet<Impression> dbSet)
		{
			IQueryable<Impression> result = dbSet;
			if (this.IDs != null && this.IDs.Any())
				result = result.Where(x => this.IDs.Contains(x.ID));
			if (!String.IsNullOrEmpty(this.UserId))
				result = result.Where(x => x.UserID.Equals(this.UserId));
			if (this.MovieId.HasValue)
				result = result.Where(x => x.Movie.Equals(this.MovieId.Value));
			return this.ApplyLimits(this.ApplyOrdering(result));
		}
	}
}
