﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace MovieRama.DataAccess
{
	public abstract class EntityCriteriaBase<T> where T : class
	{
		private List<OrderingField<T>> mOrderingFields;
		public List<OrderingField<T>> OrderingFields
		{
			get
			{
				if (this.mOrderingFields == null)
					this.mOrderingFields = new List<OrderingField<T>>();
				return this.mOrderingFields;
			}
		}

		public int Offset { get; set; }

		public int Limit { get; set; }

		protected IQueryable<T> ApplyOrdering(IQueryable<T> query)
		{
			int i = 0;
			foreach (OrderingField<T> field in this.OrderingFields)
			{
				switch (field.OrderType)
				{
					case OrderType.Ascending: query = (IQueryable<T>)(((i > 0) /*|| (i == 0 && query is IOrderedQueryable<T>)*/) ? ((IOrderedQueryable<T>)query).ThenBy(field.EntityField) : query.OrderBy(field.EntityField)); break;
					case OrderType.Descending: query = (IQueryable<T>)(((i > 0) /*|| (i == 0 && query is IOrderedQueryable<T>)*/) ? ((IOrderedQueryable<T>)query).ThenByDescending(field.EntityField) : query.OrderByDescending(field.EntityField)); break;
					default: break;
				}
				i++;
			}

			return query;
		}

		protected IQueryable<T> ApplyLimits(IQueryable<T> query)
		{
			if (this.Offset > 0)
				query = query.Skip(this.Offset);
			if (this.Limit > 0)
				query = query.Take(this.Limit);
			return query;
		}

		public abstract IQueryable<T> Collect(DbSet<T> dbSet);
	}
}
