﻿using MovieRama.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MovieRama.DataAccess
{
    public interface IMovieRepository
    {
		Movie Retrieve(int id);
		IEnumerable<Movie> RetrieveAll();
		IEnumerable<Movie> Retrieve(MovieCriteria criteria);
		void Persist(Movie movie);
		void Delete(int id);
		bool Exists(int id);
	}
}
