﻿using Microsoft.EntityFrameworkCore;
using MovieRama.Data;
using MovieRama.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MovieRama.DataAccess
{
	public class MovieRepository : IMovieRepository
	{
		private ApplicationDbContext Context;
		private DbSet<Movie> MovieEntity;

		public MovieRepository(ApplicationDbContext context)
		{
			this.Context = context;
			MovieEntity = context.Set<Movie>();
		}

		public void Delete(int id)
		{
			Movie m = this.Retrieve(id);
			if (m == null) return;

			this.Context.Remove(m);
			this.Context.SaveChanges();
		}

		public void Persist(Movie movie)
		{
			if (movie.IsStored)
				this.Context.Update(movie);
			else
			{
				this.Context.Add(movie);
			}
			this.Context.SaveChanges();
		}

		public Movie Retrieve(int id)
		{
			return this.Retrieve(new MovieCriteria() { IDs = new int[] { id } }).SingleOrDefault();
		}

		public IEnumerable<Movie> RetrieveAll()
		{
			return this.Retrieve(null);
		}

		public IEnumerable<Movie> Retrieve(MovieCriteria criteria)
		{
			IEnumerable<Movie> items = criteria == null ? this.MovieEntity.Include(x => x.User).Include(x => x.Impressions).AsEnumerable() : criteria.Collect(this.MovieEntity).AsEnumerable();
			foreach (Movie item in items)
			{
				item.IsStored = true;
			}
				
			return items;
		}

		public bool Exists(int id)
		{
			return this.Context.Movie.Any(e => e.ID == id);
		}
	}
}
