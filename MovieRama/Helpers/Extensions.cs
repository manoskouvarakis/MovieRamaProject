﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Security.Principal;
using System.Threading.Tasks;

namespace MovieRama.Helpers
{
    public static class Extensions
    {
		public static String GetUserId(this IPrincipal principal)
		{
			ClaimsPrincipal claimsPrincipal = principal as ClaimsPrincipal;
			if (claimsPrincipal == null) return null; //guest user
			return claimsPrincipal.FindFirstValue(ClaimTypes.NameIdentifier);
		}
	}
}
